from lib.hw4.dfs_bfs import Graph as DfsBfsGraph
from lib.hw4.directed_graph import Digraph
from lib.hw4.kruskals import Graph as KruskalsGraph
from lib.hw4.prims import Graph as PrimsGraph
from lib.hw4.topological_sort_SCC import Graph as TopologicalSortSCCGraph
from lib.hw4.undirected_graph import Graph as UndirectedGraph
import random
import itertools

vertex_num = 10




class TestDfsBfs(object):
    def dfs_bfs_graph(self):
        index = 2
        graph = DfsBfsGraph()
        for x in range(vertex_num - 1):
            num = random.sample(range(x + 1, vertex_num), random.randint(0, (vertex_num - x - 2) // index) + 1)
            for new in num:
                graph.addEdge(x, new)
        return graph

    def test_dfs(self):
        assert self.dfs_bfs_graph().dfs(5)

    def test_bfs(self):
        assert self.dfs_bfs_graph().bfs(5)



class TestDirectedGraph(object):
    def Directed_graph(self):
        graph = Digraph()
        edge_num = 100
        nodes = random.sample(range(100), vertex_num)

        for node in nodes:
            graph.add_node(node)

        for x in range(20, edge_num):
            graph.add_edge(random.choice(nodes), random.choice(nodes))

        return graph, nodes

    def test_exist(self):
        graph, nodes = self.Directed_graph()
        graph.add_edge(nodes[0], nodes[1])
        assert graph.contains(nodes[0])
        assert graph.has_edge(nodes[0], nodes[1])

    def test_remove_edge(self):
        graph, nodes = self.Directed_graph()
        graph.add_edge(nodes[0], nodes[1])
        graph.remove_edge(nodes[0], nodes[1])
        assert not graph.has_edge(nodes[0], nodes[1])

    def test_remove_node(self):
        graph, nodes = self.Directed_graph()
        graph.add_edge(nodes[0], nodes[1])
        graph.add_edge(nodes[2], nodes[0])
        graph.remove_node(nodes[0])
        assert not graph.has_edge(nodes[0], nodes[1])
        assert not graph.has_edge(nodes[2], nodes[0])
        assert not graph.contains(nodes[0])





mst_data = data = [(0, 7, 0.16), (2, 3, 0.17), (1, 7, 0.19), (0, 2, 0.26), (5, 7, 0.28), (1, 3, 0.29), (1, 5, 0.32),
                (2, 7, 0.34), (4, 5, 0.35), (1, 2, 0.36), (4, 7, 0.37), (0, 4, 0.38), (6, 2, 0.40), (3, 6, 0.52),
                (6, 0, 0.58), (6, 4, 0.93)]

mst_expected = {(0, 7), (2, 3), (1, 7), (0, 2), (5, 7), (4, 5), (6, 2)}

class TestKruskals(object):

    def test_kruskals(self):
        vertex_num = 6
        graph = KruskalsGraph(vertex_num)

        for v, u, w in mst_data:
            graph.addEdge(v, u, w)

        mst = graph.KruskalMST()
        assert set(mst) == mst_expected


class TestPrims(object):
    def test_prims(self):
        vertex_num = 8
        graph = PrimsGraph(vertex_num)

        for v, u, w in mst_data:
            graph.addEdge(v, u, w)

        mst = graph.primMST()
        assert set(self.swap_by_size(mst)) == set(self.swap_by_size(mst_expected))

    @staticmethod
    def swap_by_size(org):
        res = []
        for a, b in org:
            if a < b:
                res.append((a, b))
            else:
                res.append((b, a))
        return res

class TestTopologicalSortSCC(object):
    def test_topological_sort(self):
        graph = TopologicalSortSCCGraph(7)
        graph.addEdge(0, 1)
        graph.addEdge(0, 2)
        graph.addEdge(0, 5)
        graph.addEdge(1, 4)
        graph.addEdge(3, 2)
        graph.addEdge(3, 4)
        graph.addEdge(3, 5)
        graph.addEdge(3, 6)
        graph.addEdge(5, 2)
        graph.addEdge(6, 0)
        graph.addEdge(6, 4)

        expected = [3, 6, 0, 5, 2, 1, 4]
        assert graph.topological_Sort() == expected

    def test_SCC(self):
        graph = TopologicalSortSCCGraph(7)
        data = [(4, 2), (2, 3), (3, 2), (6, 0), (0, 1), (2, 0), (11, 12), (12, 9), (9, 10), (9, 11), (7, 9), (10, 12),
                (11, 4), (4, 3), (3, 5), (6, 8), (8, 6), (5, 4), (0, 5), (6, 4), (6, 9), (7, 6)]

        data.sort(key=lambda tup: tup[1])
        for a, b in data:
            graph.addEdge(a, b)

        expected = [(0, 1), (1, 0), (2, 1), (3, 1), (4, 1), (5, 1), (6, 3), (7, 4), (8, 3), (9, 2), (10, 2),
                    (11, 2), (12, 2)]
        assert graph.SCC() == expected


class TestUndirected(object):
    def test_undirected_graph(self):
        graph = UndirectedGraph()
        graph.add_vertices(["a","b","c","d","e"])
        graph.add_edges([("a", "e"), ("a", "c"), ("b", "d"), ("c", "b"), ("d", "c"), ("e", "c")])

        expected = "a: e, c\nb: d, c\nc: a, b, d, e\nd: b, c\ne: a, c\n"

        assert graph.adjacencyList() == expected